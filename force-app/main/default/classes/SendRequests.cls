// Sends Asynchronuously Messages received from trigger on AsyncRequest__c Object
// Retry with a scheduled call of this class

public without sharing class SendRequests implements queueable, Database.AllowsCallouts 
{

 	public void execute(QueueableContext context)
  {
			// On/off switch
    	if(!AppCustomSetting.appEnabled) return; 
    	
			List<AsyncRequest__c> requests;
    	
			try
    		{
	    	requests = [ SELECT ID, 
								AsyncType__c, 
								Params__c 
						FROM	AsyncRequest__c 
						WHERE 	Error__c = false
						LIMIT 1 FOR UPDATE
						];
										// TODO: do we need additional checks on CreatedById ?
										// //AND 		CreatedById = :UserInfo.getUserId() 
    		}
    		catch(Exception ex) { 
						System.debug(ex); // TODO: remove all debug statements
						return; 
			}
    	
		if(requests.size()==0) return;
    	
    	AsyncRequest__c currentRequest = requests[0];
    	
    	try
    	{
    		if(currentRequest.AsyncType__c=='Send Message')
					system.debug('***** Calling sendMessage(currentRequest)  ' + currentRequest); 
    			sendMessage(currentRequest);
    		
    		// Add more here
    		if (!Test.isRunningTest()) {
    			delete currentRequest;
			}
    		// Optional
    		database.emptyRecycleBin(new List<ID>{currentRequest.id}); 
    		
    	}
    	catch(Exception ex)
    	{
    		currentRequest.Error__c = true;
    		currentRequest.Error_Message__c = ex.getMessage();
    		update currentRequest;
    	}

    	List<AsyncRequest__c> moreRequests = [
			SELECT		ID, AsyncType__c, Params__c 
    		FROM	  	AsyncRequest__c 
    		WHERE	  	Error__c = false 
    		AND	 		ID <> :currentRequest.id 
    		LIMIT	 1 ];
    		// and	CreatedById = :UserInfo.getUserId() 
    	
    	if(moreRequests.size()==0) return;
    	
		try
		{
			enqueueSendRequests(context.getJobId());
			
		}
		catch(Exception ex)
		{
			tryToQueue();
		}
		
  }






		public static void enqueueSendRequests(ID currentJobId)
		{
			List<AsyncApexJob> jobs = [
										SELECT ID, Status, ExtendedStatus 
										FROM AsyncApexJob 
										WHERE JobType = 'Queueable'
										AND (status='Queued' OR Status='Holding') 
										AND ApexClass.Name='SendRequests' 
										AND ID!= :currentJobId Limit 1 
									  ];
					// AND CreatedById = :userinfo.getUserID() 

			if(jobs.size()==1) return;	// Already have one queued that isn't this one.
			
			system.enqueueJob(new SendRequests());
		}

    
		@future
		private static void tryToQueue()
		{
			if(!AppCustomSetting.appEnabled) return; // On/off switch
			
			try
			{
				if(Limits.getLimitQueueableJobs() - Limits.getQueueableJobs() > 0)
					enqueueSendRequests(null);
			}
			catch(Exception ex)
			{
				System.debug(ex);
				// Wait for someone else to make a request...
				// Or maybe use scheduled Apex?
			}
		}


    public void sendMessage(AsyncRequest__c request)
    {
			Integer allowedCallouts =	(Limits.getLimitCallouts() - Limits.getCallouts()) / 3; //Possibly 3 callouts per one contact record
				
			IF(allowedCallouts<=0) return;
			
			List<ID> idsAfterSplit = request.Params__c.split(',');
			
			List<Contact> ContactsToUpdate = [
											SELECT 	Id, 
													MobilePhone,
													M1_Message_to_Send__c,
													M2_Message_to_Send__c,
													M3_Message_to_Send__c,
													M1_Status__c,
													M2_Status__c,
													M3_Status__c,
													M1_Send_Time__c,
													M2_Send_Time__c,
													M3_Send_Time__c,
													M1_Message_Id__c,
													M2_Message_Id__c,
													M3_Message_Id__c
													
											FROM	Contact 
											WHERE 	ID IN :idsAfterSplit 
											AND 	(
													 M1_Status__c = 'Merged' OR
													 M2_Status__c = 'Merged' OR
													 M3_Status__c = 'Merged'
													)

											LIMIT 	:allowedCallouts
											];

			for(Contact c : ContactsToUpdate)
			{
				// c.M1_Message_to_Send__c = c.M1_Message_to_Send__c + '\n It Is Sent';
				// 	// SimulatedTranslator.translate(c.M1_Message_to_Send__c);

				// //TODO: Test performance with 3 callouts

				if(c.M1_Status__c == 'Merged'){

					IBOmniMessage m1 = new IBOmniMessage();
					
					m1.toPhoneNumber         = c.MobilePhone;
					m1.smsText               = c.M1_Message_to_Send__c;
					m1.viberText             = c.M1_Message_to_Send__c;
					m1.viberExpiryText       = 'IUTE Viber text expired';
					m1.viberButtonCaption    = 'Go to portal';
					m1.viberButtonAction     = 'www.iutecredit.com';
					m1.viberImage            = '';
					m1.sendAt                =  c.M1_Send_Time__c;
					m1.scenarioKey           = 'FF3020783693D62B01FFB124F93D6796';  //TODO: move to metadata

					// Send
					String r1 = m1.send();
					c.M1_Message_Id__c = m1.messageid;

					if (r1.contains('PENDING') ) {
						c.M1_Status__c = 'Sent';
					}
					else {
						c.M1_Status__c = 'Failed';
					}

				}

				if(c.M2_Status__c == 'Merged'){
					
					IBOmniMessage m2 = new IBOmniMessage();
					
					m2.toPhoneNumber         = c.MobilePhone;
					m2.smsText               = c.M2_Message_to_Send__c;
					m2.viberText             = c.M2_Message_to_Send__c;
					m2.viberExpiryText       = 'IUTE Viber text expired';
					m2.viberButtonCaption    = 'Go to portal';
					m2.viberButtonAction     = 'www.iutecredit.com';
					m2.viberImage            = '';
					m2.sendAt                =  c.M2_Send_Time__c;
					m2.scenarioKey           = 'FF3020783693D62B01FFB124F93D6796';  //TODO: move to metadata
					
					// Send
					String r2 = m2.send();
					c.M2_Message_Id__c = m2.messageid;

					if (r2.contains('PENDING')) {
						c.M2_Status__c = 'Sent';
					}
					else {
						c.M2_Status__c = 'Failed';
					}


				}

				if(c.M3_Status__c == 'Merged'){
					
					IBOmniMessage m3 = new IBOmniMessage();
					
					m3.toPhoneNumber         = c.MobilePhone;
					m3.smsText               = c.M3_Message_to_Send__c;
					m3.viberText             = c.M3_Message_to_Send__c;
					m3.viberExpiryText       = 'IUTE Viber text expired';
					m3.viberButtonCaption    = 'Go to portal';
					m3.viberButtonAction     = 'www.iutecredit.com';
					m3.viberImage            = '';
					m3.sendAt                =  c.M3_Send_Time__c;
					m3.scenarioKey           = 'FF3020783693D62B01FFB124F93D6796';  //TODO: move to metadata
					
					// Send
					String r3 = m3.send();
					c.M3_Message_Id__c = m3.messageid;

					if (r3.contains('PENDING') ) {
						c.M3_Status__c = 'Sent';
					}
					else {
						c.M3_Status__c = 'Failed';
					}

				}

			}
			database.update(ContactsToUpdate, false);
		}
}


// GoingAsync4.cls
/*Copyright (c) 2018, Daniel Appleman

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, 
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, 
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors 
may be used to endorse or promote products derived from this software without 
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/