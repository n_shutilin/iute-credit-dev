@isTest
global class IBOmniMessage_Test implements HttpCalloutMock {


global HttpResponse respond(HttpRequest request) {

        String requestBody = request.getBody();
        String responsebody = '{'                                                +
                                '"messages": ['				                                    +
                                    '{'				                                            +
                                        '"to": {'				                                +
                                            '"phoneNumber": "359887110033"'				        +
                                        '},'				                                    +
                                        '"status": {'				                            +
                                            '"groupId": 1,'				                        +
                                            '"groupName": "PENDING",'				            +
                                            '"id": 7,'				                            +
                                            '"name": "PENDING_ENROUTE",'				        +
                                            '"description": "Message sent to next instance"'	+
                                        '},'				                                    +
                                        '"messageId": "718fc125-375e-4b96-ba71-0dc67ceda420"'	+
                                    '}'				                                            +
                                    ']'				                                            +
                                '}';



        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody(responsebody);
        response.setStatusCode(200);
        return response; 

    }
    
    @isTest
    private static String testsend(){

 Test.startTest();
            
            Contact c1 = new Contact( Lastname='test',
                                                                            MobilePhone = '359887370075',
                                                                            M1_Message_to_Send__c = 'C1 M1 Body',
                                                                            M1_Status__c = 'Merged',
                                                                            M2_Message_to_Send__c = 'C1 M2 Body',
                                                                            M2_Status__c = 'Templated',
                                                                            M3_Message_to_Send__c = 'C1 M3 Body',
                                                                            M3_Status__c = 'Merged'
                                                                            );
            String r3 = '';

            if(c1.M3_Status__c == 'Merged'){


                 Test.setMock(HttpCalloutMock.class, new IBOmniMessageMock());
					
					IBOmniMessage m3 = new IBOmniMessage();
					
					m3.toPhoneNumber         = c1.MobilePhone;
					m3.smsText               = c1.M3_Message_to_Send__c;
					m3.viberText             = c1.M3_Message_to_Send__c;
					m3.viberExpiryText       = 'IUTE Viber text expired';
					m3.viberButtonCaption    = 'Go to portal';
					m3.viberButtonAction     = 'www.iutecredit.com';
					m3.viberImage            = '';
					m3.sendAt                =  c1.M3_Send_Time__c;
					m3.scenarioKey           = 'FF3020783693D62B01FFB124F93D6796';  //TODO: move to metadata
					// m3.transactionId         =  c1.id;
					
					r3 = m3.send();
                    //r3=  respond();                 
 
					if (r3.contains('PENDING') ) {
						c1.M3_Status__c = 'Sent';
					}
					else {
						c1.M3_Status__c = 'Failed';
					}

                	System.assertEquals('PENDING_ENROUTE', r3);
                    System.assertEquals('Sent', c1.M3_Status__c);
				
                }

Test.stopTest();



            return r3;
    }



}