/**
 * Created by yancho.karov.next on 12/6/2019.
 */

public with sharing class CustomerPoolReport {


    private static final String CONTACT_RECORD_TYPE_NAME = 'Customer';
    private static final String AGGREGATE_RESULT_COUNTRY_FILED = 'Country';
    private static final String AGGREGATE_RESULT_TOTAL_CUSTOMER_POOL_FILED = 'TotalCustomerPool';
    private static final String AGGREGATE_RESULT_DEFAULTED_CUSTOMER_POOL_FILED = 'DefaultedCustomerPool';
    private static final String AGGREGATE_RESULT_ACTIVE_CUSTOMER_POOL_FILED = 'ActiveCustomerPool';

    private Date startDate = null;
    private Date endDate = null;
    private Map<String, ResultObject> resultMap = null;
    private List<RTGrowthReportWeekly__c> weeklyReports = null;
    private List<RTGrowthReportWeekly__c> newWeeklyReports = null;

    public CustomerPoolReport(Date requestedEndDate){
        //TODO [YKA] Fix this magic number;
        this.startDate = requestedEndDate.addDays(-8);
        this.endDate = requestedEndDate.addDays(-1);
    }

    public void buildReport(){

        /**
        * 1. EXECUTE ALL CALCULATEIONS AND CREATE LIST WITH REUSLT OBJECT
        **/
        this.startCalculation();

        /**
        * 2. SELECT ALL EXISTING DATA WHERE THE executedDate is between startDate -1 AND startDate -8 //TODO [YKA] Fix me!
        **/
        this.findAllRecordsBetweenStartDateAndEndDate();

        /**
        * 3. UPDATE ALL EXISTING RECORDS FOR THE CURRENT DATE. INSERT THE NEW ONE.
        **/
        this.updateOrCreateWeeklyReportList();

        /**
        * 4. UPDATE OR INSERT ALL RECORDS
        **/
        this.updateOrInsert();
    }

    public void startCalculation(){

        resultMap = new Map<String, ResultObject>();

        /**
        * 1. Calculate Total Customer Pool
        **/
        this.calculateTotalCustomerPool();

        /**
        * 2. Calculate Defaulted Customer Pool
        **/
        this.calculateDefaultedCustomerPool();

        /**
        * 3. Calculate Active Customer Pool
        */
        this.calculateActiveCustomerPool();

    }

    private void fillResultObjects(List<AggregateResult> aggResults){


        for(AggregateResult aggResult : aggResults){

            String country = String.valueOf(aggResult.get(AGGREGATE_RESULT_COUNTRY_FILED));
            ResultObject resultObj = null;
            if(resultMap.containsKey(this.getKey(country))){
                resultObj = resultMap.get(this.getKey(country));
            }
            else{
                resultObj = new ResultObject();
                resultObj.executedDate = this.endDate;
                resultObj.country = country;
                resultMap.put(getKey(country), resultObj);
            }

            Integer totalCustomerPool = Integer.valueOf(aggResult.get(AGGREGATE_RESULT_TOTAL_CUSTOMER_POOL_FILED));
            if(totalCustomerPool != null){
                resultObj.totalCustomerPool = totalCustomerPool;
            }

            Integer defaultedCustomerPool = Integer.valueOf(aggResult.get(AGGREGATE_RESULT_DEFAULTED_CUSTOMER_POOL_FILED));
            if(defaultedCustomerPool != null){
                resultObj.defaultedCustomerPool = defaultedCustomerPool;
            }

            Integer activeCustomerPool = Integer.valueOf(aggResult.get(AGGREGATE_RESULT_ACTIVE_CUSTOMER_POOL_FILED));
            if(activeCustomerPool != null){
                resultObj.activeCustomerPool = activeCustomerPool;
            }
        }

    }

    private String getKey(String country){
        return country + '#' + this.endDate;
    }

    private void calculateTotalCustomerPool(){
        Id recordTypeId = getContactCustomerRecordType();
        List<AggregateResult> tcpResults = [SELECT COUNT(Customer_Profile__c) TotalCustomerPool,
                                                   Customer_Profile__r.Country__c Country
                                            FROM Loan_Application__c
                                            WHERE Customer_Profile__r.RecordTypeId = :recordTypeId
                                                  AND Customer_Profile__r.Test_Contact__c = false
                                                  AND sign_agreement_attached__c = true
                                                  //TODO [YKA] Extract statuses in constants;
                                                  AND status__c NOT IN ('draft', 'new', 'approved', 'rejected', 'withdrawn')
                                            GROUP BY Customer_Profile__r.Country__c];

        this.fillResultObjects(tcpResults);
    }

    private void calculateDefaultedCustomerPool(){
        Id recordTypeId = getContactCustomerRecordType();
        List<AggregateResult> dcpResults = [SELECT COUNT(Customer_Profile__c) DefaultedCustomerPool,
                                                    Customer_Profile__r.Country__c Country
                                            FROM Loan_Application__c
                                            WHERE Customer_Profile__r.RecordTypeId = :recordTypeId
                                                AND Customer_Profile__r.Test_Contact__c = false
                                                AND sign_agreement_attached__c = true
                                                //TODO [YKA] Extract statuses in constants;
                                                AND status__c IN ('defaulted_not_in_collection', 'defaulted_in_internal_collection', 'defaulted_in_collection', 'written_off')
                                                AND (term_date__c >= :this.startDate AND term_date__c <= :this.endDate)
                                            GROUP BY Customer_Profile__r.Country__c];
        this.fillResultObjects(dcpResults);
    }

    private void calculateActiveCustomerPool(){
        Id recordTypeId = getContactCustomerRecordType();
        List<AggregateResult> acpResults = [SELECT COUNT(Customer_Profile__c) ActiveCustomerPool,
                                                    Customer_Profile__r.Country__c Country
                                            FROM Loan_Application__c
                                            WHERE Customer_Profile__r.RecordTypeId = :recordTypeId
                                                AND Customer_Profile__r.Test_Contact__c = false
                                                AND sign_agreement_attached__c = true
                                                //TODO [YKA] Extract statuses in constants;
                                                AND status__c IN ('normal', 'delayed', 'closed', 'defaulted_not_in_collection', 'defaulted_in_internal_collection', 'defaulted_in_collection', 'written_off')
                                            GROUP BY Customer_Profile__r.Country__c];
        this.fillResultObjects(acpResults);
    }


    private static Id getContactCustomerRecordType() {
        return Contact.SObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get(CONTACT_RECORD_TYPE_NAME).recordTypeId;
    }

    private void findAllRecordsBetweenStartDateAndEndDate(){
        this.weeklyReports = [SELECT ID,
                            Country__c,
                            ExecutedDate__c
                         FROM RTGrowthReportWeekly__c
                         WHERE ExecutedDate__c <= :this.endDate];
    }

    private void updateOrCreateWeeklyReportList(){
        if(this.weeklyReports.isEmpty()){
            createNewWeeklyReports();
        }
        else{
            updateWeeklyReportList();
        }
    }

    private void createNewWeeklyReports(){
        this.newWeeklyReports = new List<RTGrowthReportWeekly__c>();
        List<ResultObject> resultObjectsValues =  resultMap.values();
        for(ResultObject resultObjectValue : resultObjectsValues){
            RTGrowthReportWeekly__c newReport = new RTGrowthReportWeekly__c();
            fillWeeklyReportObject(newReport, resultObjectValue);
            newWeeklyReports.add(newReport);
        }
    }

    private void updateWeeklyReportList(){
        for(RTGrowthReportWeekly__c report : this.weeklyReports){
            String key = getKey(report.Country__c);
            if(this.resultMap.containsKey(key)){
                ResultObject resultObject = this.resultMap.get(key);
                fillWeeklyReportObject(report, resultObject);
            }
        }
    }

    private void fillWeeklyReportObject(RTGrowthReportWeekly__c report, ResultObject resultObject){
        report.ExecutedDate__c = resultObject.executedDate;
        report.Country__c = resultObject.country;
        report.TotalCustomerPool__c = resultObject.totalCustomerPool;
        report.DefaultedCustomerPool__c = resultObject.defaultedCustomerPool;
        report.ActiveCustomerPool__c = resultObject.activeCustomerPool;
    }

    private void updateOrInsert(){
        if(weeklyReports.isEmpty()){
            insert this.newWeeklyReports;
        }
        else{
            update weeklyReports;
        }
    }


    public class ResultObject {
        public Date executedDate {get; set;}
        public String country {get; set;}
        public Integer totalCustomerPool {get; set;}
        public Integer defaultedCustomerPool {get; set;}
        public Integer activeCustomerPool {get; set;}
    }
}