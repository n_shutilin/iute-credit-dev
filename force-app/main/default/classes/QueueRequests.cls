//Create Message queue as records in AsyncRequest__c 
public class QueueRequests {

	// Simple protection from workflows and triggers
	private static Boolean alreadyProcessed = false;	

	public static void handleTrigger(
								List<Contact> ContactList, 
								Map<ID, Contact> newMap, 
								Map<ID, Contact> oldMap,
								TriggerOperation operation
								) {
		if(alreadyProcessed) return;
		
		alreadyProcessed = true;

		List<AsyncRequest__c> newAsyncRequests = new List<AsyncRequest__c>();
		List<String> textChangedIds = new List<ID>();


		for(Contact c : ContactList) {

			// M1/M2/M3 Message Statuses:
			// Requested
			// Templated
			// Merged ***** use this
			// Scheduled
			// Sent
			// Delivered
			// Failed
			// Cancelled
			
			// if(operation == TriggerOperation.AFTER_INSERT || c.M1_Message_to_Send__c!= oldMap.get(c.id).M1_Message_to_Send__c) 
			
			//TODO: Set M1/2/3 status to Scheduled


			if(  (c.M1_Message_to_Send__c!= oldMap.get(c.id).M1_Message_to_Send__c   &&   c.M1_Status__c == 'Merged') 	|| 				 (c.M2_Message_to_Send__c!= oldMap.get(c.id).M2_Message_to_Send__c   &&   c.M2_Status__c == 'Merged')	||(c.M3_Message_to_Send__c!= oldMap.get(c.id).M3_Message_to_Send__c   &&   c.M3_Status__c == 'Merged')  
				)
				textChangedIds.add(c.id);

			// Possibly 3 callouts per contact record 
			// Was: textChangedIds.size()>100)  
			if(textChangedIds.size()>33) 
			{
				newAsyncRequests.add(new AsyncRequest__c( AsyncType__c = 'Send Message', Params__c = string.Join(textChangedIds,',')));
				textChangedIds.clear();
			}
		}



		if(textChangedIds.size()>0)
			newAsyncRequests.add(new AsyncRequest__c(AsyncType__c = 'Send Message',
													 Params__c = string.Join(textChangedIds,',')
													 )
								);

		insert newAsyncRequests;
	}

}







// Original Copyright by Daniel Appleman:
// GoingAsync1.cls

/*Copyright (c) 2018, Daniel Appleman

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, 
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, 
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors 
may be used to endorse or promote products derived from this software without 
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/