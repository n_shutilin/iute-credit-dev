/**
 * Created by vassil.petkov@nextforcedev.com on 2020/02/03
 */
@IsTest
public class UnsubscribeEmailPageControllerTest {
    
	@IsTest
    static void testUnsubscribe() {
        Contact cont = new Contact(LastName = 'Test', Email = 'test@test.test', HasOptedOutOfEmail = false);
        insert cont;
        
        PageReference pageRef = Page.UnsubscribeEmailPage;
        pageRef.getParameters().put('Id', cont.Id);
        Test.setCurrentPageReference(pageRef);
        
        UnsubscribeEmailPageController contr = new UnsubscribeEmailPageController();
        
        contr.updateContact();
        Contact updatedContact = [SELECT HasOptedOutOfEmail FROM Contact];
        System.assertEquals(true, updatedContact.HasOptedOutOfEmail);
        
        String message = contr.getMessage();
        System.assertEquals('Your email ' + cont.Email + ' has been successfully unsubscribed!', message);
        
        contr.updateContact();
        String message2 = contr.getMessage();
        System.assertEquals('Your email has already been unsubscribed.', message2);
    }
    
    @IsTest
    static void testBosnia() {
        Contact cont = new Contact(LastName = 'Test', Email = 'test@test.test', HasOptedOutOfEmail = false, Country__c = 'Bosnia');
        insert cont;
        
        PageReference pageRef = Page.UnsubscribeEmailPage;
        pageRef.getParameters().put('Id', cont.Id);
        Test.setCurrentPageReference(pageRef);
        UnsubscribeEmailPageController contr = new UnsubscribeEmailPageController();
        contr.getFacebookLink();
        contr.getYoutubeLink();
        contr.getInstagramLink();
        contr.getUnsubscribeText();
    }
    
    @IsTest
    static void testAlbania() {
        Contact cont = new Contact(LastName = 'Test', Email = 'test@test.test', HasOptedOutOfEmail = false, Country__c = 'Albania');
        insert cont;
        
        PageReference pageRef = Page.UnsubscribeEmailPage;
        pageRef.getParameters().put('Id', cont.Id);
        Test.setCurrentPageReference(pageRef);
        UnsubscribeEmailPageController contr = new UnsubscribeEmailPageController();
    }
    
    @IsTest
    static void testMacedonia() {
        Contact cont = new Contact(LastName = 'Test', Email = 'test@test.test', HasOptedOutOfEmail = false, Country__c = 'Macedonia');
        insert cont;
        
        PageReference pageRef = Page.UnsubscribeEmailPage;
        pageRef.getParameters().put('Id', cont.Id);
        Test.setCurrentPageReference(pageRef);
        UnsubscribeEmailPageController contr = new UnsubscribeEmailPageController();
    }
    
    @IsTest
    static void testNoInput() {
        PageReference pageRef = Page.UnsubscribeEmailPage;
        pageRef.getParameters().put('Id', null);
        Test.setCurrentPageReference(pageRef);
        UnsubscribeEmailPageController contr = new UnsubscribeEmailPageController();
    }
}