@isTest
public with sharing class AppCustomSetting_Test {
        private static AppConfig__c testConfig = null;
    
    public AppCustomSetting_Test() {

          
    }

    @isTest
    public static void testObjectExists(){
         AppConfig__c cfg =  AppCustomSetting.testConfig;
         System.assertEquals(null, cfg);

    }


    @isTest
    public static void testsettings(){
       AppCustomSetting.getAppConfig();
       Boolean diag = AppCustomSetting.diagnosticsEnabled;

       System.assertEquals(diag, true);
        System.assertEquals(AppCustomSetting.appEnabled, true);

    }
}