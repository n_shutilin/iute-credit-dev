public with sharing class IBOmniMessage {


public String toPhoneNumber       {get;set;} //  '359887370075';
public String smsText             {get;set;} //  'Time to pay IUTE';
public String viberText           {get;set;} //  'Reminder: your loan payment is due' ;
public String viberExpiryText     {get;set;} //  'Message Expired';
public String viberButtonCaption  {get;set;} //  'Pay now';
public String viberButtonAction   {get;set;} //  'http://www.bg.digital/';
public String viberImage          {get;set;} //  'http://www.thesavvymarketer.net/
public Datetime sendAt            {get;set;} //   '2019-04-26T10:52:15.000+01:00'
public String scenarioKey         {get;set;} //  'FF3020783693D62B01FFB124F93D6796'; 
                                             //  '0-aff5-440d-864a-1d96c13bd4e1';
public String jsonbody;
public String result;
public String messageid;
public String status;

//Helper method to generate JSON body of the request
public String generateJSONbody(){

    // "sendAt": "2016-04-26T10:52:15.000+01:00"
    String sendAtString = '';

    if(this.sendAt > System.now().addMinutes(5)) {
        sendAtString = ',     "sendAt": "' + sendAt.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSXXX') + '"' ;
    }
    
    this.jsonbody =  '{'                                                    +
                    '"scenarioKey"  :   "' + this.scenarioKey + '",'        +
                    '"destinations" :  [{'                                  +
                            '"to":{'                                        +
                            '"phoneNumber": "'  + this.toPhoneNumber + '"'  +
                         '}'                                                +
                        '}'                                                 +
                    '],'                                                    +
                    '"viber": {'                                            +
                        '"text": "' + this.ViberText + '"'                  +
                    '},'                                                    +
                    '"sms": {'                                              +
                        '"text": "' + this.smsText + '"'                    +
                    '}'                                                     +
                    sendAtString                                            +
                '}';

    System.debug('\n*************************\njsonbody = \n' +  jsonbody + '\n*************************\n' ) ;
    return jsonbody;
}




//Main method to send omnimessage

    public String Send()
{
    generateJSONbody();
    System.debug(this.jsonbody);



    //TODO: Dow we need this here?
    // if(Test.isRunningTest()){
    //     //Call mock 
    //     IBOmniMessageMock mtest = new IBOmniMessageMock();
    //     return mtest.respond(request);
    // }


    Http http = new Http();
    HttpRequest request = new HttpRequest();

    String requestEndpoint = 'callout:InfoBip_Omnichannel/omni/1/advanced';
    // String requestEndpoint = 'https://ywen1.api.infobip.com/omni/1/advanced';
    // request.setEndpoint('callout:InfoBip_Omnichannel/v1/omni/message');
    request.setEndpoint(requestEndpoint);
    
    // Non-named credentials as in:
    // String username = '{!$Credential.Username}';
    // String password = '{!$Credential.Password}';

    //     Blob headerValue = Blob.valueOf(username + ':' + password);
    //     String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
    //     request.setHeader('Authorization', authorizationHeader);
    //     System.debug(authorizationHeader);
    //     System.debug('\n**************\n username = ' + username + ' pass = ' + password);

    String authorizationHeader = 'Basic ' + '{!$Credential.AuthorizationHeaderValue}'	;
    request.setHeader('Authorization', authorizationHeader);
    


    request.setMethod('POST');
    request.setHeader('Content-Type', 'application/json');
    request.setBody(this.jsonbody);


    try {
        HttpResponse response = http.send(request);
        System.debug('\n****************\n Result of http send:\n' + response);
            //TODO:
            // If the request is successful, parse the JSON response.
            if (response.getStatusCode() == 200) {
            // Deserialize the JSON string into collections of primitive data types.
                // Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                // Cast the values in the 'messages' key as a list
                // List<Object> messages = (List<Object>) results.get('messages');
                System.debug('\n************************\nResponse at status code 200 is: \n');

                IBomniResponse r = IBomniResponse.parse(response.getBody());
                
                // {
                //     destination={phoneNumber=359887370115
                //                 }, 
                //     messageId=d9f08f8a-0689-472b-aa72-f00d9a49763f, 
                //     status={
                //             code=0, 
                //             description=Accepted
                //             }
                // }

                System.debug(JSON.deserializeUntyped(response.getBody()));
                this.messageid = r.messages[0].messageId;
                this.status = r.messages[0].status.description;
                return r.messages[0].status.name;


            }
        } catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
             return 'error';
        }
        return 'error';
    }
}