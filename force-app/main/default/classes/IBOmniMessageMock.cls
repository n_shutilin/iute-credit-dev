@isTest
global class IBOmniMessageMock implements HttpCalloutMock {
    
    global HttpResponse respond(HttpRequest request) {

        String requestBody = request.getBody();
        String responsebody = '{'                                                +
                                '"messages": ['				                                    +
                                    '{'				                                            +
                                        '"to": {'				                                +
                                            '"phoneNumber": "359887110033"'				        +
                                        '},'				                                    +
                                        '"status": {'				                            +
                                            '"groupId": 1,'				                        +
                                            '"groupName": "PENDING",'				            +
                                            '"id": 7,'				                            +
                                            '"name": "PENDING_ENROUTE",'				        +
                                            '"description": "Message sent to next instance"'	+
                                        '},'				                                    +
                                        '"messageId": "718fc125-375e-4b96-ba71-0dc67ceda420"'	+
                                    '}'				                                            +
                                    ']'				                                            +
                                '}';



        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody(responsebody);
        response.setStatusCode(200);
        return response; 

    }
}