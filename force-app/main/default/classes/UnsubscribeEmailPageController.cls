/**
 * Created by vassil.petkov@nextforcedev.com on 2020/02/03
 */
public without sharing class UnsubscribeEmailPageController {
    private final Contact contact;
    private String message;
    private String tempId = ApexPages.currentPage().getParameters().get('Id');
    private String contactId = String.isBlank(tempId) ? '' : String.escapeSingleQuotes(tempId);
    private String facebookLink;
    private String youtubeLink;
    private String instagramLink;
    private String unsubscribeText;
    
    public UnsubscribeEmailPageController() {
        try {
        	contact = [select id, email, HasOptedOutOfEmail, Country__c from contact where id = :contactId];
        } catch (QueryException e) {
            contact = new Contact(Country__c = 'Moldova');
            message = 'Email not found.';
        }
        
        switch on contact.Country__c {
            when 'Albania' {
                facebookLink = 'https://www.facebook.com/iutecreditshqiperi/';
                youtubeLink = 'https://www.youtube.com/channel/UCR0G2qtX-jzHZICaz-_L-_g';
                instagramLink = 'https://www.instagram.com/iutecredit.al/';
                unsubscribeText = 'Çregjistrimi juaj për marrjen e emaileve nga IuteCredit Albania, u krye me sukses.';
            }
            when 'Bosnia' {
                facebookLink = 'https://www.facebook.com/iutecreditbih/';
                youtubeLink = 'https://www.youtube.com/channel/UC6MeLm0IaQuosdVlG2z0eGA';
                instagramLink = 'https://www.instagram.com/iutecreditbih/';
                unsubscribeText = 'Uspješno ste odjavljeni sa IuteCredit BH mail liste.';
            }
            when 'Macedonia' {
                facebookLink = 'https://www.facebook.com/iutecreditmacedonia/';
                youtubeLink = 'https://www.youtube.com/channel/UCqjZ0AgiC3nWA8bqjGyMFCg';
                instagramLink = 'https://www.instagram.com/iutecreditmacedonia/';
                unsubscribeText = 'Успешно се одјавивте од листата за добивање е-мејл пораки од ИутеКредит Македонија.';
            }
            //when 'Moldova' {
            when else {
                facebookLink = 'https://www.facebook.com/IuteCreditMoldova/';
                youtubeLink = 'https://www.youtube.com/channel/UC0c7AAWHqiV8-ES0Dl8B10w';
                instagramLink = 'https://www.instagram.com/iutecreditmoldova/';
                unsubscribeText = 'V-ați dezabonat cu succes de la recepționarea emailurilor de la IuteCredit Moldova.';
            }
        }
    }
    
    public String getMessage() {
        return message;
    }
    
    public String getFacebookLink() {
        return facebookLink;
    }
    
    public String getYoutubeLink() {
        return youtubeLink;
    }
    
    public String getInstagramLink() {
        return instagramLink;
    }
    
    public String getUnsubscribeText() {
        return unsubscribeText;
    }
    
    public Boolean getRenderYoutube() {
        return youtubeLink != '#';
    }
    
    public void updateContact() {
        if (contact.Id != null) {
            if (contact.HasOptedOutOfEmail) {
                message = 'Your email has already been unsubscribed.';
            } else {
                contact.HasOptedOutOfEmail = true;
                update contact;
                message = 'Your email ' + contact.Email + ' has been successfully unsubscribed!';
            }
        }
    }
}