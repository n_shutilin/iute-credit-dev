//
// Generated by JSON2Apex http://json2apex.herokuapp.com/
//

@IsTest
public class IBomniResponseTest {
	
	static testMethod void testParse() {
		String json = '{    \"messages\": [        {            \"to\": {                \"phoneNumber\": \"359887371111\"            },            \"status\": {                \"groupId\": 1,                \"groupName\": \"PENDING\",                \"id\": 7,                \"name\": \"PENDING_ENROUTE\",                \"description\": \"Message sent to next instance\"            },            \"messageId\": \"718fc125-375e-4b96-ba71-0dc67ceda420\"        }    ]}';
		IBomniResponse obj = IBomniResponse.parse(json);
		System.assert(obj != null);
	}
}