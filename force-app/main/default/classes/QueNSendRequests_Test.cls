/*Copyright (c) 2018, Daniel Appleman

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, 
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, 
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors 
may be used to endorse or promote products derived from this software without 
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
@isTest
public class QueNSendRequests_Test {

	private static Integer bulkTestSize = 10;

    @isTest
    private static void testContactsInsert() {
        // 		AppCustomSetting.testConfig = new AppConfig__c(name='default', appEnabled__c = false, enableDiagnostics__c = true);

AppCustomSetting.testConfig = new AppConfig__c(name='default', appEnabled__c = true, enableDiagnostics__c = true);
	
 		List<Contact> cts = new List<Contact>();
 		
 		for(Integer x = 0; x<bulkTestSize; x++)
    	{
    		cts.add(
    			new Contact(
                            Lastname='testLastname' + String.valueOf(x),
                            MobilePhone = '359887220011',
                            M1_Message_to_Send__c = 'C' + String.valueOf(x) + ' M1 Body',
                            M1_Status__c = 'Merged',
                            M2_Message_to_Send__c = 'C' + String.valueOf(x) + ' M2 Body',
                            M2_Status__c = 'Templated',
                            M3_Message_to_Send__c = 'C' + String.valueOf(x) + ' M3 Body',
                            M3_Status__c = 'Merged'));

    	}
    	Test.StartTest();

 		Test.setMock(HttpCalloutMock.class, 
 			new IBOmniMessageMock());
            // new IBOmniMessage_Test());

		insert cts;
		Test.StopTest();
 
 		Map<ID, Contact> ctsmap = new Map<ID, Contact>(cts);
 		
 		List<Contact> results = 
 			[Select ID, M1_Status__c, M3_Status__c
 			from Contact where ID in :ctsmap.keyset()];
 		for(Contact c: results) {
            System.debug('\nContact ' + c + ' M3_Status__c = ' + c.M3_Status__c);
 			//System.AssertEquals('Merged', c.M1_Status__c );
 			//System.AssertEquals('Merged', c.M3_Status__c );


         }
 
    }

// 	// Note: testScheduler is intended to test the scheduler code in ScheduledDispatcher and GoingAsync5
//  @isTest
//     private static void testScheduler() {
 		
// 		// Start with application off
// 		AppCustomSetting.testConfig = new AppConfig__c(name='default', appEnabled__c = false, enableDiagnostics__c = true);

//  		List<Contact> cts = new List<Contact>();
 		
//  		for(Integer x = 0; x<bulkTestSize; x++)
//     	{
//     		cts.add(
//     			new Contact(
//                             Lastname='testLastname' + String.valueOf(x),
//                             MobilePhone = '359887220011',
//                             M1_Message_to_Send__c = 'C' + String.valueOf(x) +' M1 Body',
//                             M1_Status__c = 'Merged',
//                             M2_Message_to_Send__c = 'C' + String.valueOf(x) + 'M2 Body',
//                             M2_Status__c = 'Templated',
//                             M3_Message_to_Send__c = 'C' + String.valueOf(x) + 'M3 Body',
//                             M3_Status__c = 'Merged'
//                     )
//                     );
//     	}

//  		Test.setMock(HttpCalloutMock.class, 
//  			new IBOmniMessageMock());

// 		// Application is off, so while the async records will be created, they won't run
// 		insert cts;

//     	// Test.StartTest();
// 		// // Turn the app back on
// 		// AppCustomSetting.testconfig.appEnabled__c = true;
// 		// // And run the scheduler (it will run on StopTest)
// 		// GoingAsync5.startScheduler(DateTime.now(), 'testjob');

// 		// Test.StopTest();
 
//  		// Map<ID, Contact> ctsmap = new Map<ID, Contact>(cts);
 		
//         // List<Contact> results = 
//  		// 	[Select ID, M1_Status__c, M3_Status__c
//  		// 	from Contact where ID in :ctsmap.keyset()];
//  		// for(Contact c: results) {
//  		// 	System.AssertEquals(c.M1_Status__c,  
//         //      'Sent');
//  		// 	System.AssertEquals(c.M3_Status__c,  
//         //      'Sent');

 
//     }	

}