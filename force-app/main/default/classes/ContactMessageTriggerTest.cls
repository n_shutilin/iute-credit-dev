@isTest
private class ContactMessageTriggerTest {
	@isTest
	private static void ContactMessageTriggerTest() {

		List<Contact> cts = new List<Contact>();


		for(Integer x = 0; x<10; x++)
		{
			cts.add(
				new Contact(
					Lastname='testLastname' + String.valueOf(x+1),
					MobilePhone = '3598' + String.valueOf(x+1) + String.valueOf(x+1) + '220011'
					)
				);

		}
		Test.StartTest();

		Test.setMock(HttpCalloutMock.class,
		             new IBOmniMessageMock());
		// new IBOmniMessage_Test());

		insert cts;

		List<Contact> ctsupdated = new List<Contact>();
		integer i = 1;
		for (Contact c : cts) {
			i++;
			c.M1_Message_to_Send__c = 'C' + String.valueOf(i) + ' M1 Body';
			c.M1_Status__c = 'Merged';
			c.M2_Message_to_Send__c = 'C' + String.valueOf(i) + ' M2 Body';
			c.M2_Status__c = 'Templated';
			c.M3_Message_to_Send__c = 'C' + String.valueOf(i) + ' M3 Body';
			c.M3_Status__c = 'Merged';
			ctsupdated.add(c);

		}
		update ctsupdated;
		System.debug('\n********** ctsupdated = \n' + ctsupdated);
		List<Contact> ctscheck = [SELECT Id, Lastname, MobilePhone, M1_Message_to_Send__c, M2_Message_to_Send__c, M3_Message_to_Send__c, M1_Status__c, M2_Status__c, M3_Status__c FROM Contact WHERE ID IN :ctsupdated];

        System.debug('\n********** ctsupdated = \n' + ctsupdated);
        System.debug('\n********** contactsinserted = \n' + ctscheck);

		Test.StopTest();
		System.assertEquals(cts.size(), 10);
        System.assertEquals(ctscheck.size(), ctsupdated.size());


    }
}