global class BatchAggregateSelect implements Database.Batchable<sObject>, Database.Stateful {
	global Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        return Database.getQueryLocator('select MAX(Id) ma, master__r.id from Detail__c group by master__r.id');
    }
    global void execute(Database.BatchableContext bc, AggregateResult[] groupedResults){
        // process each batch of records
        System.debug(groupedResults[0].get('ma'));
    }   
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }
}