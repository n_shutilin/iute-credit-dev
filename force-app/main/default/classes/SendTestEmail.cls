public class SendTestEmail {
    
    public static void execute() {
        
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>{};
            
            for (Contact c : [SELECT id, Email, AccountId, id_card_nr__c, Country__c FROM Contact WHERE HasOptedOutOfEmail = false]) {
                for (EmailTemplate e : [Select Id,
                                        name,
                                        Subject,
                                        Description,
                                        HtmlValue,
                                        DeveloperName,
                                        Body 
                                        from EmailTemplate 
                                        where name IN ('Albania', 
                                                       'Bosnia',
                                                       'Macedonia', 
                                                       'Moldova') ]) {
                                                           
                                                           if(c.Country__c == e.Name) {
                                                           
                                                               Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                                                               
                                                               message.setTargetObjectId(c.id);
                                                               message.setSenderDisplayName('Support'); 
                                                               message.setReplyTo('support_iutecredit@next-consult.com');
                                                               message.setUseSignature(false);
                                                               message.setBccSender(false);
                                                               message.setSaveAsActivity(false);
                                                               
                                                               message.setTemplateID(e.Id); 
                                                               message.setWhatId(c.AccountId); //This is important for the merge fields in template to work
                                                               List<String> toAddresses = new List<String>();
                                                               
                                                               if (c.Email != null) {toAddresses.add(c.Email);}
                                                               
                                                               if (c.id_card_nr__c != null) {toAddresses.add(c.id_card_nr__c);}
                                                               
                                                               message.toAddresses = toAddresses;
                                                               messages.add(message);
                                                           }
                                                       }
            }
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
    }
}