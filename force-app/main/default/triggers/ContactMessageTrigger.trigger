trigger ContactMessageTrigger on Contact (after update) {
    // Trigger disabled on 2019.07.30, remove this logic to enable
    If (Test.isRunningTest()) {
        QueueRequests.handleTrigger(trigger.new,
                trigger.newMap,
                trigger.oldMap,
                trigger.operationType
        );
    }
}